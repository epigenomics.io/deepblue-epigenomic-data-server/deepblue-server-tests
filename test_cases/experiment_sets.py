import time

import helpers

from deepblue_client import DeepBlueClient
import data_info

class TestExperimentSets(helpers.TestCase):

  def test_experiment_sets(self):
    epidb = DeepBlueClient(address="localhost", port=31415)
    self.init_full(epidb)

    epidb.list_experiments

    (s, ees) = epidb.list_experiments("", "", "", "", "", "", "", self.admin_key)
    (s, names) = epidb.extract_names(ees)

    self.assertEquals(['hg19_chr2_1', 'hg18_chr1_1', 'deepshitty', 'hg19_chr1_1', 'hg19_chr1_2', 'hg19_chr1_3', 'hg19_small'], names)


    (s, es1) = epidb.create_experiments_set("Test 1", "set test one", True, names, self.admin_key)

    (s, info_es1) = epidb.info(es1, self.admin_key)
    self.assertEquals([{'experiments': ['hg19_chr2_1', 'hg18_chr1_1', 'deepshitty', 'hg19_chr1_1', 'hg19_chr1_2', 'hg19_chr1_3', 'hg19_small'], 'public': True, '_id': 'es1', 'name': 'Test 1', 'description': 'set test one'}], info_es1)
