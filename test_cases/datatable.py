import helpers

from deepblue_client import DeepBlueClient


class TestDatatable(helpers.TestCase):
  ##
  ## 'Path collision at sample_info: generic server error'.
  ##
  def test_datatable_path_collision_bug(self):
    epidb = DeepBlueClient(address="localhost", port=31415)
    self.init_base(epidb)

    sample_id = self.sample_ids[0]

    format = ",".join([
      "CHROMOSOME",
      "START",
      "END",
      "NAME",
      "SCORE",
      "STRAND",
      "SIGNAL_VALUE",
      "P_VALUE",
      "Q_VALUE",
      "PEAK"
    ])
    eid = None
    with open("data/bed/hg19_chr1_1.bed") as f:
      res, eid = epidb.add_experiment("exp1", "hg19", "Methylation", sample_id, "tech1",
                                      "ENCODE", "desc1", f.read(), format,
                                      {"foo":"bar", "extra":"123"}, self.admin_key)
      self.assertSuccess(res, eid)

      res, eid = epidb.add_experiment("exp2", "hg19", "Methylation", sample_id, "tech1",
                                      "ENCODE", "desc1", f.read(), format,
                                      {"foo.x":"bar", "extra":"123"}, self.admin_key)
      self.assertSuccess(res, eid)

    parameters = (
        "experiments",
        ["name", "data_type", "epigenetic_mark", "sample_info", "upload_info", "biosource"],
        0,
        20,
        None,
        "biosource",
        "desc",
        True,
        {},
        self.admin_key
    )

    status, t = epidb.datatable(*parameters)
    self.assertSuccess(status, t)
    self.assertEqual(2, t[0])
