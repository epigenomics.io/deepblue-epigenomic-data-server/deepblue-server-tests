import time

import helpers

from deepblue_client import DeepBlueClient
import data_info
import gzip


class TestExperiments(helpers.TestCase):

  def test_load_bedgraph(self):
    epidb = DeepBlueClient(address="localhost", port=31415)
    self.init_base(epidb)

    sample_id = self.sample_ids[0]
    regions_data = gzip.open("data/bedgraph/chr19.txt.gz").read()

    # adding two experiments with the same data should work
    res = epidb.add_experiment("S0022IH2.ERX300681.H3K36me3.bwa.GRCh38.20150528.bedgraph", "hg19", "Methylation", sample_id, "tech1",
              "ENCODE", "desc1", regions_data, "bedgraph", {"md5sum": "afd4af5afd5afd4af5afd5afd4af5afd5"}, self.admin_key)
    self.assertSuccess(res)

    (status, query_id) = epidb.select_regions ("#afd4af5afd5afd4af5afd5afd4af5afd5", None, None, None, None, None, "chr19", 49388217, 49417994, self.admin_key)

    self.assertSuccess(status, query_id)

    (status, input) = epidb.input_regions("hg19", "chr19\t49388217\t49417994", self.admin_key)
    self.assertSuccess(status, input)

    (status, query_overlap) = epidb.intersection(query_id, input, self.admin_key)
    self.assertSuccess(status, query_overlap)

    (status, request_id) = epidb.get_regions(query_id, "CHROMOSOME,START,END,VALUE", self.admin_key)
    self.assertSuccess(status, request_id)
    (status, overlap_request_id) = epidb.get_regions(query_id, "CHROMOSOME,START,END,VALUE", self.admin_key)
    self.assertSuccess(status, overlap_request_id)

    by_select = self.get_regions_request(request_id)
    by_overlap = self.get_regions_request(overlap_request_id)

    self.assertEqual(by_overlap, by_select)
    self.assertTrue(len(by_select) > 0)

    (status, info) = epidb.info("#afd4af5afd5afd4af5afd5afd4af5afd5", self.admin_key)
    self.assertEquals(info[0]["_id"], "e1")

  def test_experiments_preview(self):
    epidb = DeepBlueClient(address="localhost", port=31415)
    self.init_base(epidb)

    sample_id = self.sample_ids[0]
    regions_data = helpers.load_bed("hg19_chr1_1")
    format = data_info.EXPERIMENTS["hg19_chr1_1"]["format"]

    # adding two experiments with the same data should work
    res = epidb.add_experiment("test_exp1", "hg19", "Methylation", sample_id, "tech1",
              "ENCODE", "desc1", regions_data, format, None, self.admin_key)
    self.assertSuccess(res)

    expected = 'CHROMOSOME\tSTART\tEND\tNAME\tSCORE\tSTRAND\tSIGNAL_VALUE\tP_VALUE\tQ_VALUE\tPEAK\nchr1\t713240\t713390\t.\t0.0000\t+\t21.0000\t69.6000\t-1.0000\t-1\nchr1\t713520\t713670\t.\t0.0000\t-\t21.0000\t22.4866\t-1.0000\t-1\nchr1\t713900\t714050\t.\t0.0000\t+\t59.0000\t71.2352\t-1.0000\t-1\nchr1\t714160\t714310\t.\t0.0000\t+\t22.0000\t101.8740\t-1.0000\t-1\nchr1\t714540\t714690\t.\t0.0000\t+\t77.0000\t105.3120\t-1.0000\t-1'

    status, preview = epidb.preview_experiment('test_exp1', self.admin_key)
    self.assertEqual(expected, preview)

  def test_experiments_pass(self):
    epidb = DeepBlueClient(address="localhost", port=31415)
    self.init_base(epidb)

    sample_id = self.sample_ids[0]
    regions_data = helpers.load_bed("hg19_chr1_1")
    format = data_info.EXPERIMENTS["hg19_chr1_1"]["format"]

    # adding two experiments with the same data should work
    res = epidb.add_experiment("test_exp1", "hg19", "Methylation", sample_id, "tech1",
              "ENCODE", "desc1", regions_data, format, None, self.admin_key)
    self.assertSuccess(res)

    res = epidb.add_experiment("test_exp2", "hg19", "Methylation", sample_id, "tech1",
              "ENCODE", "desc1", regions_data, format, None, self.admin_key)
    self.assertSuccess(res)

    res, experiments = epidb.list_experiments("hg19", "peaks", None, "NO_BIOSOURCE", None, None, None, self.admin_key)
    self.assertSuccess(res, experiments)
    self.assertEqual(len(experiments), 0)

    res, experiments = epidb.list_experiments(None, None, None, None, None, None, None, self.admin_key)
    self.assertSuccess(res, experiments)
    self.assertEqual(len(experiments), 2)

    res, experiments = epidb.list_experiments("hg19", "peaks", None, "K562", None, None, None, self.admin_key)
    self.assertSuccess(res, experiments)
    self.assertEqual(len(experiments), 2)

    experiments_names = [x[1] for x in experiments]

    self.assertTrue("test_exp1" in experiments_names)
    self.assertTrue("test_exp2" in experiments_names)

    s, ids = epidb.name_to_id(['test_exp1'], 'experiments', self.admin_key)
    self.assertEqual(ids, [['e1', 'test_exp1']])
    s, ids = epidb.name_to_id(['test_exp1', 'test_exp2'], 'experiments', self.admin_key)
    self.assertEqual([['e1', 'test_exp1'], ['e2', 'test_exp2']], ids)
    s, ids = epidb.name_to_id('test_exp1', 'experiments', self.admin_key)
    self.assertEqual([['e1', 'test_exp1']], ids)

  def test_insert_local_file(self):
    epidb = DeepBlueClient(address="localhost", port=31415)
    self.init_base(epidb)
    sample_id = self.sample_ids[0]

    # adding two experiments with the same data should work
    d = helpers.get_tests_dir()
    data_path = d + "wig/scores1.wig"
    (res, _id) = epidb.add_experiment("test_exp1", "hg19", "Methylation", sample_id, "tech1",
              "ENCODE", "desc1", "", "wig", {"__local_file__": data_path}, self.admin_key)
    self.assertSuccess(res, _id)

    res = epidb.add_experiment("test_exp1", "hg19", "Methylation", sample_id, "tech1",
              "ENCODE", "desc1", "", "wig", {"__local_file__": "inexistent_file.wig"}, self.admin_key)
    self.assertFailure(res)

    res, experiments = epidb.list_experiments("hg19", "signal", None, "K562", None, None, None, self.admin_key)
    self.assertSuccess(res, experiments)
    self.assertEqual(len(experiments), 1)

    res, qid1 = epidb.select_regions("test_exp1", "hg19", None, None, None, None, None, None, None, self.admin_key)
    self.assertSuccess(res, qid1)
    (s, req1) = epidb.get_regions(qid1, "CHROMOSOME,START,END", self.admin_key)
    self.assertSuccess(s, req1)
    data1 = self.get_regions_request(req1)

    res, qid1 = epidb.select_regions(_id, None, None, None, None, None, None, None, None, self.admin_key)
    self.assertSuccess(res, qid1)
    (s, req2) = epidb.get_regions(qid1, "CHROMOSOME,START,END", self.admin_key)
    self.assertSuccess(s, req2)
    data2 = self.get_regions_request(req2)

    self.assertEqual(data1, data2)

  def test_double_experiment_same_user_fail(self):
    epidb = DeepBlueClient(address="localhost", port=31415)
    self.init_base(epidb)

    uid, user_key = self.insert_user(epidb, "test_user")
    s, tmp_user = epidb.modify_user_admin(uid, "permission_level", "INCLUDE_EXPERIMENTS", self.admin_key)
    self.assertSuccess(s)

    sample_id = self.sample_ids[0]
    regions_data = helpers.load_bed("hg19_chr1_1")
    format = data_info.EXPERIMENTS["hg19_chr1_1"]["format"]

    # adding the same experiments with different users should work
    exp = ("test_exp1", "hg19", "Methylation", sample_id, "tech1",
           "ENCODE", "desc1", regions_data, format, None)

    res = epidb.add_experiment(*(exp + (self.admin_key,)))
    self.assertSuccess(res)

    res = epidb.add_experiment(*(exp + (user_key,)))
    self.assertFailure(res)
    self.assertEqual(res[1], "Experiment name 'test_exp1' is already being used.")


  def test_double_experiment_fail(self):
    epidb = DeepBlueClient(address="localhost", port=31415)
    self.init_base(epidb)

    sample_id = self.sample_ids[0]
    regions_data = helpers.load_bed("hg19_chr1_1")
    format = data_info.EXPERIMENTS["hg19_chr1_1"]["format"]


    # adding the same experiment with the same user should fail
    exp = ("test_exp1", "hg19", "Methylation", sample_id, "tech1",
              "ENCODE", "desc1", regions_data, format, None)

    res = epidb.add_experiment(*(exp + (self.admin_key,)))
    self.assertSuccess(res)

    res = epidb.add_experiment(*(exp + (self.admin_key,)))
    self.assertFailure(res)


  def test_add_with_invalid_sample(self):
    epidb = DeepBlueClient(address="localhost", port=31415)
    self.init_base(epidb)

    sample_id = self.sample_ids[0]
    regions_data = helpers.load_bed("hg19_chr1_1")
    format = data_info.EXPERIMENTS["hg19_chr1_1"]["format"]

    res = epidb.add_experiment("test_exp1", "hg19", "Methylation", "_s123", "tech1",
              "ENCODE", "desc1", regions_data, format, None, self.admin_key)
    self.assertFailure(res)


  def test_add_with_invalid_genome(self):
    epidb = DeepBlueClient(address="localhost", port=31415)
    self.init_base(epidb)

    sample_id = self.sample_ids[0]
    regions_data = helpers.load_bed("hg19_chr1_1")
    format = data_info.EXPERIMENTS["hg19_chr1_1"]["format"]

    res = epidb.add_experiment("test_exp1", "_hg123", "Methylation", sample_id, "tech1",
              "ENCODE", "desc1", regions_data, format, None, self.admin_key)
    self.assertFailure(res)


  def test_add_with_invalid_project(self):
    epidb = DeepBlueClient(address="localhost", port=31415)
    self.init_base(epidb)

    sample_id = self.sample_ids[0]
    regions_data = helpers.load_bed("hg19_chr1_1")
    format = data_info.EXPERIMENTS["hg19_chr1_1"]["format"]

    res = epidb.add_experiment("test_exp1", "hg19", "Methylation", sample_id, "tech1",
              "No project", "desc1", regions_data, format, None, self.admin_key)
    self.assertFailure(res)


  def test_add_with_invalid_chromosome(self):
    epidb = DeepBlueClient(address="localhost", port=31415)
    self.init_base(epidb)

    sample_id = self.sample_ids[0]
    regions_data = helpers.load_bed("hg18_invalid_chr")
    _format = data_info.EXPERIMENTS["hg19_chr1_1"]["format"]

    res = epidb.add_experiment("test_exp1", "hg19", "Methylation", sample_id, "tech1",
              "ENCODE", "desc1", regions_data, _format, None, self.admin_key)
    self.assertFailure(res)
    self.assertEquals("Unable to find the chromosome 'chrQ' in the genome 'hg19'.", res[1])

    res = epidb.add_experiment("test_exp1", "hg19", "Methylation", sample_id, "tech1",
              "ENCODE", "desc1", regions_data, _format, {"__ignore_unknow_chromosomes__": True}, self.admin_key)
    self.assertSuccess(res)

    res, q_exp = epidb.select_regions("test_exp1", "hg19", None, None, None, None, None, None, None,self.admin_key)
    res, req = epidb.get_regions(q_exp, _format, self.admin_key)

    data = self.get_regions_request(req)

    regions_data_okay = helpers.load_bed("hg18_invalid_chr_okay")

    self.assertEqual(data, regions_data_okay)

  def test_add_with_out_of_bound_region(self):
    epidb = DeepBlueClient(address="localhost", port=31415)
    self.init_base(epidb)

    sample_id = self.sample_ids[0]
    regions_data = helpers.load_bed("hg18_out_of_bounds")
    _format = data_info.EXPERIMENTS["hg19_chr1_1"]["format"]

    res = epidb.add_experiment("test_exp1", "hg19", "Methylation", sample_id, "tech1",
              "ENCODE", "desc1", regions_data, _format, None, self.admin_key)
    self.assertFailure(res)

    res = epidb.add_experiment("test_exp1", "hg19", "Methylation", sample_id, "tech1",
              "ENCODE", "desc1", regions_data, _format, {"__trim_to_chromosome_size__": True}, self.admin_key)
    self.assertSuccess(res)

    res, q_exp = epidb.select_regions("test_exp1", "hg19", None, None, None, None, None, None, None,self.admin_key)
    res, req = epidb.get_regions(q_exp, _format, self.admin_key)

    data = self.get_regions_request(req)

    regions_data_okay = helpers.load_bed("hg18_out_of_bounds_okay")

    self.assertEqual(data, regions_data_okay)


  def test_add_with_invalid_epigenetic_mark(self):
    epidb = DeepBlueClient(address="localhost", port=31415)
    self.init_base(epidb)

    sample_id = self.sample_ids[0]
    regions_data = helpers.load_bed("hg19_chr1_1")
    format = data_info.EXPERIMENTS["hg19_chr1_1"]["format"]

    res = epidb.add_experiment("test_exp1", "hg19", "No Epigenetic ", sample_id, "tech1",
              "ENCODE", "desc1", regions_data, format, None, self.admin_key)
    self.assertFailure(res)

  def test_shitty_deep_file(self):
    epidb = DeepBlueClient(address="localhost", port=31415)
    self.init_base(epidb)

    eid1 = self.insert_experiment(epidb, "deepshitty")
    res, qid1 = epidb.select_regions("deepshitty", "hg18", None, None, None,
        None, None, None, None, self.admin_key)
    self.assertSuccess(res, qid1)

    res, req = epidb.get_experiments_by_query(qid1, self.admin_key)
    exps = self.get_regions_request(req)
    self.assertEqual(len(exps), 1)
    (res, req) = epidb.get_regions(qid1, "CHROMOSOME,START,END", self.admin_key)
    exps = self.get_regions_request(req)
    self.assertEqual(exps, "chr1\t62125\t62154")

  def test_get_by_query(self):
    epidb = DeepBlueClient(address="localhost", port=31415)
    self.init_base(epidb)

    eid1 = self.insert_experiment(epidb, "hg18_chr1_1")
    eid2 = self.insert_experiment(epidb, "hg19_chr1_1")
    eid3 = self.insert_experiment(epidb, "hg19_chr1_2")
    eid4 = self.insert_experiment(epidb, "hg19_chr1_3")

    res, qid1 = epidb.select_regions("hg18_chr1_1", "hg18", None, None, None,
        None, None, None, None, self.admin_key)
    self.assertSuccess(res, qid1)

    res, req = epidb.get_experiments_by_query(qid1, self.admin_key)
    exps = self.get_regions_request(req)
    self.assertEqual(len(exps), 1)
    self.assertEqual(exps, {eid1: "hg18_chr1_1"})

    (res, req) = epidb.get_regions(qid1, "CHROMOSOME,START,END", self.admin_key)
    exps = self.get_regions_request(req)

    self.assertEqual(exps, 'chr1\t683240\t690390\nchr1\t697520\t697670\nchr1\t702900\t703050\nchr1\t714160\t714310\nchr1\t714540\t714690\nchr1\t715060\t715210\nchr1\t761180\t761330\nchr1\t762060\t762210\nchr1\t762420\t762570\nchr1\t762820\t762970\nchr1\t763020\t763170\nchr1\t839540\t839690\nchr1\t840080\t840230\nchr1\t840600\t840750\nchr1\t858880\t859030\nchr1\t859600\t859750\nchr1\t860240\t860390\nchr1\t861040\t861190\nchr1\t875400\t875550\nchr1\t875900\t876050\nchr1\t876400\t876650\nchr1\t877900\t878050\nchr1\t879180\t880330')


    (res, req) = epidb.get_regions(qid1, "CHROMOSOME,START,END,@STRAND", self.admin_key)
    exps = self.get_regions_request(req)
    self.assertEqual(exps, 'chr1\t683240\t690390\t+\nchr1\t697520\t697670\t-\nchr1\t702900\t703050\t+\nchr1\t714160\t714310\t+\nchr1\t714540\t714690\t+\nchr1\t715060\t715210\t+\nchr1\t761180\t761330\t-\nchr1\t762060\t762210\t+\nchr1\t762420\t762570\t.\nchr1\t762820\t762970\t-\nchr1\t763020\t763170\t-\nchr1\t839540\t839690\t+\nchr1\t840080\t840230\t+\nchr1\t840600\t840750\t-\nchr1\t858880\t859030\t.\nchr1\t859600\t859750\t.\nchr1\t860240\t860390\t+\nchr1\t861040\t861190\t-\nchr1\t875400\t875550\t+\nchr1\t875900\t876050\t-\nchr1\t876400\t876650\t+\nchr1\t877900\t878050\t-\nchr1\t879180\t880330\t-')


    res, qid2 = epidb.select_regions(None, "hg19", "Methylation", None, None,
        None, None, None, None, self.admin_key)
    self.assertSuccess(res, qid1)

    res, req = epidb.get_experiments_by_query(qid2, self.admin_key)
    self.assertSuccess(res, req)
    exps = self.get_regions_request(req)

    self.assertSuccess(res, exps)
    self.assertEqual(len(exps), 3)
    self.assertEqual({'e4': 'hg19_chr1_3', 'e3': 'hg19_chr1_2', 'e2': 'hg19_chr1_1'}, exps)
    self.assertTrue(eid2 in exps)
    self.assertTrue(eid3 in exps)
    self.assertTrue(eid4 in exps)

    (res, req) = epidb.get_regions(qid2, "CHROMOSOME,START,END", self.admin_key)
    x = self.get_regions_request(req)
    self.assertEqual(x, 'chr1\t567500\t567650\nchr1\t713000\t713070\nchr1\t713240\t713390\nchr1\t713280\t713430\nchr1\t713520\t713670\nchr1\t713520\t713670\nchr1\t713900\t714050\nchr1\t713920\t714070\nchr1\t714160\t714310\nchr1\t714200\t714350\nchr1\t714300\t714350\nchr1\t714460\t714590\nchr1\t714540\t714690\nchr1\t714540\t714690\nchr1\t715060\t715210\nchr1\t715080\t715230\nchr1\t719100\t719330\nchr1\t761180\t761330\nchr1\t762060\t762210\nchr1\t762060\t762210\nchr1\t762100\t762250\nchr1\t762420\t762570\nchr1\t762440\t762590\nchr1\t762460\t762500\nchr1\t762620\t762790\nchr1\t762820\t762970\nchr1\t762820\t762970\nchr1\t763020\t763170\nchr1\t839540\t839690\nchr1\t840000\t840150\nchr1\t840080\t840230\nchr1\t840100\t840310\nchr1\t840600\t840750\nchr1\t840640\t840790\nchr1\t840850\t840990\nchr1\t857740\t857890\nchr1\t858740\t858890\nchr1\t858880\t859030\nchr1\t859600\t859750\nchr1\t859640\t859790\nchr1\t859650\t859790\nchr1\t860220\t860370\nchr1\t860240\t860390\nchr1\t860600\t860750\nchr1\t861040\t861190\nchr1\t861040\t861190\nchr1\t875220\t875370\nchr1\t875400\t875550\nchr1\t875900\t876050\nchr1\t875920\t876070\nchr1\t876180\t876330\nchr1\t876420\t876570\nchr1\t877000\t877150')

  def test_incomplete_chromosome(self):
    mm10_info = '''chr1 195471971
chr2  182113224
chrX  171031299
chr3  160039680
chr4  156508116
chr5  151834684
chr6  149736546
chr7  145441459
chr10 130694993
chr8  129401213
chr14 124902244
chr9  124595110
chr11 122082543
chr13 120421639
chr12 120129022
chr15 104043685
chr16 98207768
chr17 94987271
chrY  91744698
chr18 90702639
chr19 61431566
chr5_JH584299_random  953012
chrX_GL456233_random  336933
chrY_JH584301_random  259875
chr1_GL456211_random  241735
chr4_GL456350_random  227966
chr4_JH584293_random  207968
chr1_GL456221_random  206961
chr5_JH584297_random  205776
chr5_JH584296_random  199368
chr5_GL456354_random  195993
chr4_JH584294_random  191905
chr5_JH584298_random  184189
chrY_JH584300_random  182347
chr7_GL456219_random  175968
chr1_GL456210_random  169725
chrY_JH584303_random  158099
chrY_JH584302_random  155838
chr1_GL456212_random  153618
chrUn_JH584304  114452
chrUn_GL456379  72385
chr4_GL456216_random  66673
chrUn_GL456393  55711
chrUn_GL456366  47073
chrUn_GL456367  42057
chrUn_GL456239  40056
chr1_GL456213_random  39340
chrUn_GL456383  38659
chrUn_GL456385  35240
chrUn_GL456360  31704
chrUn_GL456378  31602
chrUn_GL456389  28772
chrUn_GL456372  28664
chrUn_GL456370  26764
chrUn_GL456381  25871
chrUn_GL456387  24685
chrUn_GL456390  24668
chrUn_GL456394  24323
chrUn_GL456392  23629
chrUn_GL456382  23158
chrUn_GL456359  22974
chrUn_GL456396  21240
chrUn_GL456368  20208
chrM  16299
chr4_JH584292_random  14945
chr4_JH584295_random  1976'''

    epidb = DeepBlueClient(address="localhost", port=31415)
    self.init_base(epidb)

    sample_id = self.sample_ids[0]

    res = epidb.add_genome("mm10", "Mouse genome 10", mm10_info, self.admin_key)
    self.assertSuccess(res)

    data = "1\t10\t30\nUn_GL456392\t123\t1234\nM\t1234\t2000"

    res = epidb.add_experiment("test_exp1", "mm10", "Methylation", sample_id, "tech1",
              "ENCODE", "desc1", data, "CHROMOSOME,START,END", None, self.admin_key)

    self.assertSuccess(res)

    (status, q) = epidb.select_regions('test_exp1', "mm10", None, None, None, None, None, None, None, self.admin_key)
    (status, req) = epidb.get_regions(q, "CHROMOSOME,START,END", self.admin_key)

    regions = self.get_regions_request(req)
    expected = "chr1\t10\t30\nchrM\t1234\t2000\nchrUn_GL456392\t123\t1234"
    self.assertEqual(regions, expected)
