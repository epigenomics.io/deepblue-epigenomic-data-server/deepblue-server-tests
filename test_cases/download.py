import helpers

from deepblue_client import DeepBlueClient


class TestDownload(helpers.TestCase):

  def test_download_command(self):
    epidb = DeepBlueClient(address="localhost", port=31415)
    self.init_base(epidb)

    file_data = open("data/cpgIslandExtFull.bed").read()

    cpg_island =  ",".join([
      "CHROMOSOME",
      "START",
      "END",
      "NAME",
      "LENGTH",
      "NUM_CPG",
      "NUM_GC",
      "PER_CPG",
      "PER_CG",
      "OBS_EXP"
    ])

    REGIONS_COUNT = 28691

    res = epidb.add_annotation("Cpg Islands", "hg19", "CpG islands", file_data, cpg_island, None, self.admin_key)
    self.assertSuccess(res)

    res, qid_1 = epidb.select_annotations("Cpg Islands", "hg19", None, None, None, self.admin_key)
    self.assertSuccess(res, qid_1)
    self.assertEqual(qid_1, 'q1')

    res, req = epidb.count_regions(qid_1, self.admin_key)
    self.assertSuccess(res, req)
    count = self.count_request(req)
    self.assertEqual(REGIONS_COUNT, count)

    res, req = epidb.get_regions(qid_1, "CHROMOSOME,START,END,NAME,LENGTH,NUM_CPG,NUM_GC,PER_CPG,PER_CG,OBS_EXP", self.admin_key)
    regions = self.get_regions_request(req)

    req_count = len(regions.split("\n"))
    self.assertEqual(REGIONS_COUNT, req_count)

    import urllib2
    import bz2
    def http_get(req, key):
        url = 'http://127.0.0.1:31415/download?r_id='+req+'&key='+key
        req = urllib2.Request(url)
        response = urllib2.urlopen(req)
        response = response.read()
        return bz2.decompress(response)

    d_regions = http_get(req, self.admin_key)
    self.assertEqual(REGIONS_COUNT, len(d_regions.split("\n")))