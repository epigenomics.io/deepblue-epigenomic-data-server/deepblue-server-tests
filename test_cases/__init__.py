## Admin commands
import init
import admin
import user_auth
import modify_user
import list_commands
import list_requests
import xml

# Bugs
import bugs

## Metadata
import column_types
import epigenetic_marks
import biosources
import samples
import techniques
import projects
import permissions
import genomes
import sequence
import remove
import states
import permissions
import changes
import clone
import list_similar
import faceting

## Annotations
import annotations
import tiling
import input_regions

##Experiments
import experiments

##Commands
import info
import search

##Operations
import select_regions
import intersection
import overlaps
import filters
import merge
import flank
import extend
import aggregate
import binning
import distinct
import coverage
import complex_queries
import patterns
import output
import score_matrix
import enrichment
import experiment_sets
import cancel
import datatable

## Download /download
import download

##Parsers
import bedgraph_parser
import wig_parser

##Genes
import gene_models
import gene_ontology

##Extras
import lua



#import janitor # annoying test, I have to find out how to set up the janitor parameters evenwhen the test fails
#import gene_expressions # Slow tests.