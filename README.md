It is necessary to checkout the DeepBlue Python repository one directory under this one.
Ex:
```
./DeepBlue/deepblue-tests
./DeepBlue/deepblue-python
```

### Environmental Variables
| Name | Description
| --- | ---
| TEST_DATA_PATH | origin path containing the test file scores1.wig, this file should also exist in the deepblue-server under the same path.

