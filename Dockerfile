FROM python:2.7

RUN python2.7 -m pip install pymongo==2.8.1

WORKDIR /home/
RUN git clone https://gitlab.com/epigenomics.io/deepblue-epigenomic-data-server/deepblue-server-tests.git
WORKDIR ./deepblue-server-tests
RUN rm -f deepblue_client.py
RUN wget https://gitlab.com/epigenomics.io/deepblue-epigenomic-data-server/deepblue-python/-/raw/main/src/deepblue_client.py

# ENTRYPOINT ["tail"]
# CMD ["-f", "/dev/null"]
ENTRYPOINT ["python2.7"]
CMD ["test.py"]